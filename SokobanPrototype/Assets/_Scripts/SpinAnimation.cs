﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A component that can be added to objects to animate them spinning with a script by calling DoSpinAnimation coroutine
/// </summary>
public class SpinAnimation : MonoBehaviour
{
    
    /// <summary>
    /// This Coroutine will animate an object to spin into the air and fall back down with randomization each time
    /// </summary>
    /// <param name="speedMod">How fast the spinning animation happens</param>
    /// <param name="widthMod">How wide the spinning may fly to the left or right</param>
    /// <param name="heightMod">How high the spinning may fly into the air</param>
    /// <param name="destroyWhenDone">Tell the animation to destroy the GameObject it is attached to when finished if this is true</param>
    /// <returns></returns>
    public IEnumerator DoSpinAnimation(float speedMod, float widthMod, float heightMod, bool destroyWhenDone)
    {
        float lerp = 0f;

        Vector3 startPos = transform.position;
        float height = Random.Range(1.0f, 2.5f) * heightMod;
        float width = Random.Range(-2.5f, 2.5f) * widthMod;

        float spin = Random.Range(500f, 1000f);
        if (width < 0f)
            spin = -spin;

        while (lerp < 1f)
        {
            lerp = Mathf.MoveTowards(lerp, 1f, Time.deltaTime * speedMod);

            transform.Rotate(-Vector3.forward * Time.deltaTime * spin);

            float yRoot = (lerp - .5f) * 2f;
            transform.position = startPos + new Vector3(lerp * width, (yRoot * yRoot) * -height + height, 0f);

            yield return null;
        }

        if (destroyWhenDone)
            Destroy(gameObject);
    }
}
