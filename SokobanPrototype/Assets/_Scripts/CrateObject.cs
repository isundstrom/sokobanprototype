﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Crates are MoveableObjects that can be crushed and can optionally contain keys inside them.
/// </summary>
public class CrateObject : MoveableObject
{
    public GameObject brokenPrefab;//Prefab object to be spawned when broken
    public GameObject keyContained;//Scene reference to the key object we contained (can be null if no key contained)

    public GameObject keyStamp;//Graphic object turned on to show there's a key inside this crate if there is one


    public override void Start()
    {
        //If we contain a key
        if (keyContained != null)
        {
            keyContained.SetActive(false);//Hide key until broken
            keyStamp.SetActive(true);
        }

        base.Start();
    }

    public override void Crush()
    {
        //When crushed, create broken crate object here
        Instantiate(brokenPrefab, transform.position, Quaternion.identity);//Create broken pieces where I was crushed

        //If we contain a key...
        if (keyContained != null)//Move key to crate's position and activate (like it was inside when crushed)
        {
            keyContained.transform.position = transform.position;
            keyContained.gameObject.SetActive(true);
        }

        Destroy(gameObject);


    }

}
