﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// Teleporters are objects on the ground that teleport things (players or crates) put on top of them
/// </summary>
public class TeleporterObject : LevelObject
{
    public TeleporterObject connectedTeleporter;//Ref to the other teleporter in the level that this teleports to or from

    public Vector2 lastTeleportDirection;//The movement direction of the thing we are teleporting when it got pushed into us, used to move it out of the sent to teleporter correctly
    public GameObject teleportObjectCopy;//A cached reference of the object to copy if the teleporter is looped and turned into a cloning device

    bool usedThisTurn = false;//Whether we teleported during this turn or not

    public override void Awake()
    {
        //Add ourself to the GC's list of teleporters
        GameController.gC.teleporterObjects.Add(this);
        base.Awake();
    }

    protected override void OnDestroy()
    {
        //Remove ourself from the GC's list of teleporters
        GameController.gC.teleporterObjects.Remove(this);
        base.OnDestroy();
    }

    /// <summary>
    /// A frame called by gC during resolving turn, that spawns things from looping teleporters (cloning) and moves them before anything else happens
    /// </summary>
    public void TeleporterSpawnFrame()
    {
        
        if (looping && lastVerb != Verbs.Resting)//If we are set to looping and not resting we should try to clone something
        {
            for (int i = 0; i < GameController.gC.levelObjects.Count; i++)
            {//Search through all objects
                if (GameController.gC.levelObjects[i] != this)//Don't push self
                {
                    if (Vector3.Distance(GameController.gC.levelObjects[i].transform.position, transform.position) < 1f)
                    {//Found regular object at position
                        if (GameController.gC.levelObjects[i].blocking)
                        {//Teleporter is blocked don't spawn anything
                            return;
                        }
                    }
                }
            }

            //Didn't find anyhing blocking teleporter so go ahead and spawn clone
            GameObject copy = Instantiate(teleportObjectCopy);
            copy.SetActive(true);
            MoveableObject mO = copy.GetComponent<MoveableObject>();
            mO.Start();
            mO.Awake();
            //Start and awake calls adding the object to the object pool a second time so remove it...
            GameController.gC.levelObjects.Remove(mO);
            if (mO as Player)
                GameController.gC.playerList.Remove(mO as Player);//If the spawned object is a Player also remove that
            mO.teleportSpawnSickness = true;

            mO.Move(lastTeleportDirection, true);//Move the object in the direction cached

        }
        
    }
    
    /// <summary>
    /// Called when something is moved onto a teleporter, will cache the teleporting vars so that looping works
    /// </summary>
    /// <param name="objectTeleporting"></param>
    /// <param name="dir"></param>
    public void SetTeleportationDetails(MoveableObject objectTeleporting, Vector2 dir)
    {
        //Make a copy of the object we just made so that we can recreate it later if set to loop
        teleportObjectCopy = Instantiate(objectTeleporting.gameObject) as GameObject;
        teleportObjectCopy.transform.position = transform.position;
        MoveableObject mO = teleportObjectCopy.GetComponent<MoveableObject>();
        GameController.gC.levelObjects.Remove(mO);//Make sure gC doesn't have this object in it's object pool
        teleportObjectCopy.SetActive(false);//Hide away, not actually used in game until copied again

        lastTeleportDirection = dir;

        //Set verb based on direction:
        if (dir.x > .1f)
            SetVerb(Verbs.MoveRight);
        else
            if (dir.x < -.1f)
            SetVerb(Verbs.MoveLeft);
        else
            if (dir.y > .1f)
            SetVerb(Verbs.MoveUp);
        else
            if (dir.y < -.1f)
            SetVerb(Verbs.MoveDown);

        usedThisTurn = true;

    }

    public override void EndFrame()
    {
        if (!usedThisTurn && !looping && teleportObjectCopy != null)
        {
            //Clear saved teleport if looping is not on:
            Destroy(teleportObjectCopy);
            SetVerb(Verbs.Resting);
        }

        usedThisTurn = false;
        base.EndFrame();
    }


}
