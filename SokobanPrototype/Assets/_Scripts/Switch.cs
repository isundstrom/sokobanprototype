﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Switches are LevelObjects that can be turned on or off. When turned on they inform their array of toggledObjects to change their state accordingly
/// The Switch class is used both for button switches that are covered by the player or crates, but also by keys which behave like a one off switch before being deactivated
/// This is mucky: in an ideal world there would probably be an extra Key class and a Button class that both inherit from Switch
/// Significantly, a Switch is one of the few LevelObjects that can't have looping magic cast on it.
/// </summary>
public class Switch : LevelObject
{
    public bool isKey = false;//Whether this Switch is just a key

    public int switchIndex = 0;//Each switch is given an index, which is used to set its color

    public Color[] colors;//The colors switches should be based on their color
    

    public SwitchToggledObject[] toggledObjects;//What objects the switch toggles when on or off (drag and drop in the editor)

    public bool coveredThisFrame = false;//Whether the switch was first covered this frame (indicating the need to change states this frame)
    
    public Sprite pressedSprite;//What sprite to use when the Switch is toggled on
    Sprite unpressedSprite;//What the Switch's sprite gets set to when off
    bool keyCollected = false;//If we are a key, have we already been collected?

    bool on = false;//Whether we are in an on or off state



    public override void Awake()
    {
        //Set ups sprite references, sprites, and colors based on our switchIndex
        spriteRenderer = GetComponent<SpriteRenderer>();

        spriteRenderer.color = colors[switchIndex];
        unpressedSprite = spriteRenderer.sprite;//Save so we can toggle back after being pressed

        //Loop through all our toggledObjects and assign their color to match ours:
        for (int i = 0; i < toggledObjects.Length; i++)
        {
            toggledObjects[i].SetColor(colors[switchIndex], isKey);
        }

        base.Awake();
        
    }

    public override void Start()
    {
        

        base.Start();


    }

    public override void SetCoveredOnThisFrame(MoveableObject callingMO)//callingMO is used by teleporter to clone the mO
    {
        coveredThisFrame = true;
    }

    public override void BeginFrame()
    {
        //After movement check if we are covered or collected:
        if (isKey)
        {
            //=======[CHECK IF PLAYER COLLECTED KEY THIS FRAME]======
            for (int p = 0; p < GameController.gC.playerList.Count; p++)//Because of cloning mechanic, there could be multiple players!!!
            {
                if (Vector3.Distance(GameController.gC.playerList[p].GetEndPos(), transform.position) < .1f || keyCollected)//Player is going to end on key this frame or we've already been collected
                {
                    //Set state to on and toggle on objects
                    on = true;
                    for (int i = 0; i < toggledObjects.Length; i++)
                    {
                        toggledObjects[i].Toggle(on);
                    }
                }
            }
        }
        else
        {
            //=======[CHECK IF BUTTON SWITCH COVERED THIS FRAME]======
            if (on != coveredThisFrame)//This frame our state changed
            {//Switch in state
                on = coveredThisFrame;
                spriteRenderer.sprite = (on) ? pressedSprite : unpressedSprite;//Toggle graphic
            }

            //Make sure all our toggled objects match our on/off state:
            for (int i = 0; i < toggledObjects.Length; i++)
            {
                toggledObjects[i].Toggle(on);
            }
        }

    }

    public override void SetFramePos(float lerp)
    {
        //Only animation for switches is that halfway through a movement frame the key dissapears.
        if (isKey)
        {
            //Deactivate me when player halfway on top:
            if (on && lerp > .5f && !keyCollected)
            {
                GetComponent<SpriteRenderer>().enabled = false;
                keyCollected = true;
            }
        }
        
    }


    public override void EndFrame()
    {
        //Set covered this frame back to its default state:
        coveredThisFrame = false;
    }

}
