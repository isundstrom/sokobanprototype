﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class for objects that can be toggled on and off from switches (such as buttons or keys). Examples include doors and conveyor belts
/// </summary>
public class SwitchToggledObject : LevelObject
{
    public GameObject keySprite;//A child game object that is just a sprite of a key to indicate the object is effected by a key, not a button

    protected bool currentState = false;//Whether we are currently on or off
    protected bool stateFromSwitch = false;//Whether the Switch that controls us is currently on and off (might not match currentState if locked on or off from looping magic)

    public override void Awake()
    {
        base.Awake();
    }

    public override void Start()
    {
        base.Start();
    }

    /// <summary>
    /// Sets the color of the SwitchToggleObject's sprite to match the switch. If being set by a key will also toggle on keySprite
    /// </summary>
    /// <param name="color"></param>
    /// <param name="keyBased"></param>
    public virtual void SetColor(Color color, bool keyBased)
    {
        spriteRenderer.color = color;
        
        if (keySprite != null)
            keySprite.SetActive(keyBased);
    }

    /// <summary>
    /// Called by the Switch to tell us to toggle on or off if able (overwritten by child classes)
    /// </summary>
    /// <param name="on"></param>
    public virtual void Toggle(bool on)
    {

    }
}
