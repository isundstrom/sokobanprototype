﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

/// <summary>
/// Class that stores all data about the current level's map. Used to check for collisions with tilesets, holes, or cracks
/// </summary>
public class MapData : MonoBehaviour
{
    static public MapData mD;//Singleton ref

    public Tilemap wallsMap;//Reference to Tilemap of tiles that are walls
    public Tilemap holesMap;//Reference to Tilemap of tiles that are holes
    public Tilemap holeCoversMap;//Reference to Tilemap of tiles that cracks covering up holes

    // Start is called before the first frame update
    void Awake()
    {
        mD = this;//Set singleton
    }
    
    /// <summary>
    /// Checks the map data to see if the tile within the given map exists at the given position
    /// </summary>
    /// <param name="tilemapToCheck"></param>
    /// <param name="pos"></param>
    /// <returns></returns>
    public static bool CheckTileSetForCollision(Tilemap tilemapToCheck, Vector3Int pos)
    {
        Tile.ColliderType collider = tilemapToCheck.GetColliderType(pos);//Get collider within tilemap at position
        if (collider == Tile.ColliderType.None)//There isn't a collider
        {
            return false;
        }
        else//There is a collider
            return true;
    }

    /// <summary>
    /// Will destroy a tile at the given position within the given tile map (used to remove holeCovers (cracks)), but could be used to delete walls theoretically
    /// </summary>
    /// <param name="tilemapToDestroy"></param>
    /// <param name="pos"></param>
    public static void DestroyTileAtPosition(Tilemap tilemapToDestroy, Vector3Int pos)
    {
        tilemapToDestroy.SetTile(pos, null);
    }
    
}
