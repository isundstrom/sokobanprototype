﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Doors are objects that can be opened and closed by switches. However, they are also castable, they can be looped to continuously animate opening and closing turning them
/// into a crate/player crusher OR they can be locked open or closed to get them to ignore what their switch tells them.
/// </summary>
public class DoorObject : SwitchToggledObject
{
    public Transform doorSlide;//The door object sprite we move when we open and close

    public bool startOpen = false;//Whether the door begins the level opened or closed
    float openPosHeight = .7f;//How high the door is when opened
    
    bool openThisTurn = false;//Whether the door got opened this frame
    bool closeThisTurn = false;//Whether the door got closed this frame

    public override void Start()
    {
        //Make sure door state/appearance matches whether we startOpen or not
        if (startOpen)
        {
            doorSlide.transform.localPosition = new Vector3(0f, openPosHeight, 0f);//Set door to appear open
            blocking = false;
        }

        base.Start();
    }

    public override void Toggle(bool on)
    {
        //Whent toggled, open or close the door appropriately.

        stateFromSwitch = on;
        
        openThisTurn = closeThisTurn = false;//Reset openThisTurn and closeThisTurn

        if (!looping)
        {
            //========[OPENING/CLOSING WHEN NOT LOOPING]==============
            if (currentState != stateFromSwitch)//We aren't already in the state the Switch wants us to be
            {
                if (stateFromSwitch)//If switch turned on
                {
                    if (startOpen)
                        closeThisTurn = true;//If we start the level open, turning on the Switch closes us
                    else
                        openThisTurn = true;//If we start the level closed, turning on the Switch opens us
                }
                else
                {
                    if (startOpen)
                        openThisTurn = true;//If we start the level open, turning off the Switch opens us
                    else
                        closeThisTurn = true;//If we start the level closed, turning off the Switch closes us
                }

                //Make sure verbs match whether we are being opened or closed this turn:
                if (openThisTurn)
                    SetVerb(Verbs.MoveUp);//MoveUP indicates the door opened
                if (closeThisTurn)
                    SetVerb(Verbs.MoveDown);//MoveDown indicates the door closed
            }
            else
            {
                //If there was no change from the switch, we were resting this turn:
                SetVerb(Verbs.Resting);
            }

        }
        else
        {
            //========[LOOPING BEHAVIORS]==============
            //Looping states:
            if (lastVerb == Verbs.Resting)
            {
                //Nothing happens if looping resting
                return;
            }
            else
                if (lastVerb == Verbs.MoveDown)
            {
                //If looping a closing verb, we continuously close over and over
                closeThisTurn = true;
            }
            else
            if (lastVerb == Verbs.MoveUp)
            {
                //If looping an opening verb, we continuously open over and over
                openThisTurn = true;
            }
        }

        if (openThisTurn)
        {
            //If opening this turn, check for crushing before the frame animation starts
            CheckForCrush();
        }

    }

    /// <summary>
    /// Breaks any objects in the door's way or kills them. Called when opening and closing the door
    /// </summary>
    void CheckForCrush()
    {
        
        LevelObject objAtPosition = null;
        for (int i = 0; i < GameController.gC.levelObjects.Count; i++)
        {//Search through all objects
            if (GameController.gC.levelObjects[i] != this && GameController.gC.levelObjects[i].crushable)//Don't crush self
            {
                if (Vector3.Distance(GameController.gC.levelObjects[i].transform.position, transform.position) < .1f)
                {//Found object at position
                    objAtPosition = GameController.gC.levelObjects[i];
                    break;
                }
            }
        }

        if (objAtPosition != null)//If we found an object at our position, try to crush it
            objAtPosition.Crush();
    }

    public override void SetFramePos(float lerp)
    {
        //Animate the door opening and closing if necessary
        
        if (!openThisTurn && !closeThisTurn)
            return;//Neither opening and closing so don't animate
        
        //Lerp y position the masked door sprite that gives look of opening and closing
        float start = (closeThisTurn) ? openPosHeight : 0f;
        float end = (closeThisTurn) ? 0f:openPosHeight;

        doorSlide.transform.localPosition = new Vector3(0f, Mathf.Lerp(start, end, lerp), 0f);
    }

    public override void EndFrame()
    {
        //When finished, save current state and update whether blocking or not
        if (!looping)
        {
            currentState = stateFromSwitch;


            if (currentState)
            {
                blocking = startOpen;//A door that starts open is closed and blocking when "on", but open and a free space when "off"
            }
            else
                blocking = !startOpen;//A door that starts closed is closed and blocking when "off", but open and a free space when "on"
        }else
        {
            if (lastVerb != Verbs.Resting)
                blocking = false;//When moving up or down, door is never blocking, it just crushes stuff
        }


        if (closeThisTurn)
        {
            CheckForCrush();
        }
    }


    protected override void SetVerb(Verbs verb)
    {
        if (!castable)//Non castable objects don't have verbs
            return;

        if (looping)//Can't change verb when looping
            return;

        //Remember the verb for looping purposes
        lastVerb = verb;

        //Set the verb sprite based on verb
        verbIcon.sprite = verbSpriteIcons[(int)(verb)];//Kinda garbage to cast enum to int, because if you move movement verbs from start this won't work anymore
    }


    public override void CastSpellOn()
    {
        if(lastVerb != Verbs.Resting)
            blocking = false;
        base.CastSpellOn();
    }

    public override void CancelSpellOn()
    {
        
        base.CancelSpellOn();

        /*
        if (stateFromSwitch != currentState)
        {
            if (stateFromSwitch)
                StartCoroutine(ToggleOn());

            if (!stateFromSwitch)
                StartCoroutine(ToggleOff());
        }
        */
    }
}
