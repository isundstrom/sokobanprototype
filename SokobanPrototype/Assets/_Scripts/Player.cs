﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

/// <summary>
/// Script attached to Players. There can be multiple players, since cloning is possible. Player inherits from MoveableObject
/// </summary>
public class Player : MoveableObject
{


    public GameObject rangeIndicator;//Scene reference to togglable blue square sprite that indicates how far the player's magic can be cast

    public GameObject targetter;//Scene reference to box that indicates which grid space we choosing to target with magic

    public LevelObject castingObject;//Stored ref to the object we have cast looping magic on

    public float targetterRange = 2f;//How far and wide our targetting magic extends in grid spaces

    public Transform graphic;//Ref to our child graphic object that is turned to face different directions

    public bool selectingTarget = false;//Whether or not we are currently selecting a target or not
    

    public override void Awake()
    {
        base.Awake();
    }

    // Start is called before the first frame update
    public override void Start()
    {
        //Add ourself to the GC's player list:
        GameController.gC.playerList.Add(this);
        base.Start();
    }

    protected override void OnDestroy()
    {
        //Remove ourself from the GC's player list when we leave the scene:
        GameController.gC.RemovePlayer(this);
        base.OnDestroy();
    }
    
    /// <summary>
    /// Checks if we are on top of the stairs of the level and goes to the next level if we are.
    /// </summary>
    void CheckIfOnExit()
    {
        if ((GameController.gC.stairsObject.transform.position - transform.position).magnitude < Mathf.Epsilon)
        {//On the stairs
            GameController.gC.ChangeLevel(1);//Go to next level
        }
    }

    public override void EndFrame()
    {
        CheckIfOnExit();
        base.EndFrame();
    }

    /// <summary>
    /// Called when we click on a target to cast magic on it
    /// </summary>
    /// <returns></returns>
    public bool SelectingTargetClicked()
    {
        if (selectingTarget)//If selecting
        {
            ToggleSelectTarget();

            if (AttemptSpellCast())//If true, we successfully casted on an object
            {
                //Casting counts as a turn
                return true;//Tell gC to move turn forward
            }
            else
            {//What we clicked didn't cast on anything, so treat that as cancelling the spell on any actively looping objects instead
                if (castingObject != null)//If there is something casted on now
                {
                    castingObject.CancelSpellOn();//Cancel spell on it
                    castingObject = null;//Clear
                    return true;//Tell gC to move turn forward
                }
            }


        }
        else
            ToggleSelectTarget();//If we weren't selecting a target turn on targetting

        return false;//Tell gC NOT to move turn forward
    }


    /// <summary>
    /// Called when we click on a space while targetting opened. Returns true if chose a valid target and false if clickd on an empty square
    /// </summary>
    /// <returns></returns>
    bool AttemptSpellCast()
    {
        //Calculate world position to check at using local position of targetter:
        Vector3 testPos = transform.position + targetter.transform.localPosition;
        LevelObject objAtPosition = null;
        int highestOrder = -100;//In the case of multiple objects in one grid space (such as a crate on a conveyor belt), pick the one highest in sprite render order
        for (int i = 0; i < GameController.gC.levelObjects.Count; i++)
        {//Search through all objects
            if (GameController.gC.levelObjects[i] != this && GameController.gC.levelObjects[i].castable)//Something I can cast at here
            {
                if (Vector3.Distance(GameController.gC.levelObjects[i].transform.position, testPos) < .1f)
                {//Found object at position
                    if (GameController.gC.levelObjects[i].spriteRenderer.sortingOrder > highestOrder)//If this has a higher sortingOrder than the best one so far
                    {
                        objAtPosition = GameController.gC.levelObjects[i];//Save me as the object found so far
                        highestOrder = objAtPosition.spriteRenderer.sortingOrder;//Save my sortingOrder as the best so far
                    }
                }
            }
        }
        
        if (objAtPosition != null)//If we did find a object
        {
            if (castingObject != null)
                castingObject.CancelSpellOn();//Cancel the magic on the last object if there is one

            objAtPosition.CastSpellOn();//Tell newly found object to loop

            castingObject = objAtPosition;//Save ref to object we are using magic on

            return true;
        }
        else
            return false;//Didn't find an object
    }

    /// <summary>
    /// Called by the GC to place the magic selecting target box where the mouse is
    /// </summary>
    /// <param name="mousePos"></param>
    public void MoveTargetterWithMouse(Vector2 mousePos)
    {
        Vector2 local = new Vector2(mousePos.x - transform.position.x, mousePos.y - transform.position.y);//Use local position relative to player to set the child targetter position
        if (Mathf.Abs(local.x) < targetterRange + .1f && Mathf.Abs(local.y) < targetterRange + .1f)//Only allow if within targetting range
        {
            targetter.transform.localPosition = new Vector3(local.x, local.y, 0f);//Actually set targetter position
        }
    }

    /// <summary>
    /// Moves the magic selecting target box using orthogonal direcitonal movement (if only playing with controller)
    /// </summary>
    /// <param name="dir"></param>
    public void MoveTargetter(Vector2 dir)
    {
        Vector3 checkPos = targetter.transform.localPosition + new Vector3(dir.x, dir.y, 0f);


        if (Mathf.Abs(checkPos.x) < targetterRange + .1f && Mathf.Abs(checkPos.y) < targetterRange + .1f)
        {
            //Actually move, since in range:
            targetter.transform.localPosition = checkPos;
        }
    }

    /// <summary>
    /// Toggles on and off whether we are currently selecting a target with our magic
    /// </summary>
    void ToggleSelectTarget()
    {
        selectingTarget = !selectingTarget;

        SetSelectTargetGraphics();
    }

    /// <summary>
    /// Updates visuals based on targetting state (turns on blue squares, etc)
    /// </summary>
    void SetSelectTargetGraphics()
    {
        rangeIndicator.SetActive(selectingTarget);
        targetter.SetActive(selectingTarget);
        if (selectingTarget)//Only recenter when turning on
            targetter.transform.localPosition = Vector3.zero;

        GameController.gC.ToggleVerbIcons(selectingTarget);//Turn on or off the verb icons
    }

    public override void Crush()
    {
        //Play an animation of the player spinning up into the air if we are crushed
        SpinAnimation spinAnimation = GetComponent<SpinAnimation>();
        spinAnimation.StartCoroutine(spinAnimation.DoSpinAnimation(2f, 1f, 2f, false));
        markedDead = true;
        Invoke("Die", 1f);//Wait a second before dying
    }
    

    /// <summary>
    /// Called when we are crushed or fall in a hole
    /// </summary>
    void Die()
    {
        //gC will call game over if this is the only player on the field:
        Destroy(gameObject);
    }
}
