﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConveyorObject : SwitchToggledObject
{

    public bool startOn = true;//Whether the conveyor is on at the beginning of the level or not

    public Transform beltsHolder;//Transform holding belt sprites that is moved within a mask to create illusion of endless belt shifting
    public SpriteRenderer secondBeltPieceSR;//We need an additional sprite reference for the conveyor for tinting purposes (ideally LevelObject's spriteRenderer should be an array)
    public Sprite offSprite;//Sprite for how the conveyor belt looks when turned off (darker)
    Sprite onSprite;//Sprite for how the conveyor belt looks when turned on (lighter looking)

    List<MoveableObject> objectsToMove;//What objects we moved this frame

    public override void Awake()
    {
        //Add ourself to GC's list of all conveyor objects in the scene:
        GameController.gC.conveyorObjects.Add(this);

        onSprite = spriteRenderer.sprite;//Cache our starting sprite as the onSprite

        SetBeltState(startOn);//Update our state to reflect startOn bool
        
        base.Awake();
    }

    protected override void OnDestroy()
    {
        //Remove ourself from GC's conveyor belt list when we leave the scene:
        GameController.gC.conveyorObjects.Remove(this);
        base.OnDestroy();
    }

    public override void Toggle(bool on)
    {
        stateFromSwitch = on;

        if (!looping)
        {
            currentState = stateFromSwitch;

        }

        //Update our current state based on being toggled by a switch and whether we started on or off:
        bool setMoving = (currentState && !startOn) | (!currentState && startOn);

        SetBeltState(setMoving);
            
    }

    public override void SetFramePos(float lerp)
    {
        if (lastVerb != Verbs.Resting)
            beltsHolder.transform.localPosition = Vector3.Lerp(Vector3.zero, Vector3.up, lerp);//Slide the conveyor belt for illusion of belts moving
    }

/// <summary>
/// Called to toggle the conveyor belt on or off. Ensures the correct verb is set and the sprites are updated to look on or off.
/// </summary>
/// <param name="on"></param>
    void SetBeltState(bool on)
    {
        if (on)
        {
            SetVerb(Verbs.MoveUp);
            spriteRenderer.sprite = onSprite;
            secondBeltPieceSR.sprite = onSprite;
        }
        else
        {
            SetVerb(Verbs.Resting);
            spriteRenderer.sprite = offSprite;
            secondBeltPieceSR.sprite = offSprite;
        }
    }
    
    public override void SetColor(Color color, bool keyBased)
    {
        //Tint both belt pieces based on associated switch color:
        secondBeltPieceSR.color = color;
        base.SetColor(color, keyBased);
    }

    protected override void SetVerb(Verbs verb)
    {
        if (!castable)//Non castable objects don't have verbs
            return;

        if (looping)//Can't change verb when looping
            return;

        //Remember the verb for looping purposes
        lastVerb = verb;

        //Set the verb sprite based on verb
        verbIcon.sprite = (verb == Verbs.Resting)?verbSpriteIcons[0]: verbSpriteIcons[1];//Kinda garbage to cast enum to int, because if you move movement verbs from start this won't work anymore
    }

    /// <summary>
    /// Searches and find if an object is resting on the conveyor this frame, saves it's info, marks it as moved by a conveyor, telling other objects moved by conveyors not to push it
    /// </summary>
    public void ConveyorGetRestingObject()
    {
        if (lastVerb == Verbs.Resting)//Don't move objects if off
            return;


        objectsToMove = new List<MoveableObject>();

        //Search for object on top of belt
        for (int i = 0; i < GameController.gC.levelObjects.Count; i++)
        {//Search through all objects
            if (GameController.gC.levelObjects[i] as MoveableObject)//Only moves moveable objects.
            {
                MoveableObject mO = GameController.gC.levelObjects[i] as MoveableObject;
                Vector3 endPos = mO.GetEndPos();
                if (Vector3.Distance(endPos, transform.position) < .1f)
                {//Found object at position
                    objectsToMove.Add(mO);
                    mO.fromBelowMove = transform.up;//Set the fromBelow movement for the MO to match my local direction
                }
            }
        }
        
    }

    /// <summary>
    /// Function for conveyors called by GC that ensures all DoBelowMovement() functions for MoveableObjects effected by each conveyor gets called
    /// </summary>
    public void ConveyorFrame()
    {
        if (lastVerb == Verbs.Resting)//Don't move objects if off
            return;
        
        //Move objects
        foreach(MoveableObject mO in objectsToMove)
        {
            mO.DoBelowMovement();
        }
    }
}
