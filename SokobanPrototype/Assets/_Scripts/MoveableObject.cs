﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Base class for LevelObjects that can be moved (pushable by the player)
/// </summary>
public class MoveableObject : LevelObject
{
    /// <summary>
    /// A class that stores the intended movements of the MoveableObject on a given "frame". A Moveable object might have multiple movements in one frame,
    /// for example, if a crate is pushed down on to a right facing conveyor belt, it will have a downward Movement AND a rightward Movement
    /// Once all the various Movements for all MoveableObjects are logically resolved and stored, they are then animated all together at once using a series of SetFramePos() by the GC
    /// </summary>
    class Movement
    {
        public bool teleportation = false;//Whether teleported this frame
        public Vector2 startPos;//Where we started at in this frame
        public Vector2 endPos;//Our ending positoin this frame

        public Movement(Vector2 start, Vector2 end, bool teleporting)
        {
            startPos = start;
            endPos = end;
            teleportation = teleporting;
        }
    }


    public bool heavyAndCrackTiles = false;//Whether this object will break and fall through floor tiles that are cracked in the level or not

    public Vector2 fromBelowMove = Vector2.zero;//Direction of any movement caused by a object below us this frame (such as a conveyor belt)

    public bool teleportSpawnSickness = false;//Prevent some behavior of this object if it just spawned from a teleport (set back to false at endframe)


    List<Movement> movementsThisTurn;//The list of movements that GC tells us we'll be taking this turn
    int nonTeleportMovementsCount;//How many movements we took this turn that aren't teleportation, used to determine how quickly our animation should happen 
    //(since teleporting happens instantly they can't count towards the total)

    Vector3 startPos;//Where we started this frame.
    public override void Awake()
    {
        if (spriteRenderer == null)//If not manually set
            spriteRenderer = GetComponent<SpriteRenderer>();

        movementsThisTurn = new List<Movement>();
        base.Awake();
    }

    public override void Start()
    {
        startPos = transform.position;
    }

    /// <summary>
    /// Returns the final positoin the object will be at at the end of the frame based on any and all movements
    /// </summary>
    /// <returns></returns>
    public Vector3 GetEndPos()
    {
        if (movementsThisTurn.Count > 0)
            return movementsThisTurn[movementsThisTurn.Count - 1].endPos;//Either return the last movement made or...

        return startPos;//Return our starting position, bc no movements yet.
    }

    /// <summary>
    /// Tell the object to calculate effects of below movement (movement from conveyor belts)
    /// </summary>
    public void DoBelowMovement()
    {
        if (fromBelowMove.magnitude > .1f)
            Move(fromBelowMove, true);
    }

    /// <summary>
    /// This function resolves a potential movement on the MoveableObject. If the movement is into free space it will add the movement to the MoveableObject's list of Movements this frame.
    /// It will also recognize the object being pushed into any spaces occupied by other Moveable objects and in turn recursively call Move() to push those.
    /// Returns true if the object successfully moved into the new space or false if it failed.
    /// </summary>
    /// <param name="dir">Direction of the movement</param>
    /// <param name="belowMovement">Whether or not the movement is being caused by something below (ie. a conveyor belt)</param>
    /// <returns></returns>
    public bool Move(Vector2 dir, bool belowMovement)
    {
        

        if (markedDead)//Don't push things marked to be destroyed
            return true;
        
        
        if (looping && lastVerb == Verbs.Resting)
        {
            return false;//An object that is cast on and looping resting acts like a wall
        }

        Vector3 startPos = GetEndPos();//Start position of a new movement should be the end position of all past movements

        //Calculate Potential space to move to:
        Vector3 moveTo = startPos + new Vector3(dir.x, dir.y, 0f);

        Vector3Int moveToInt = Utility.V3ToV3Int(moveTo);

        if (belowMovement)
            fromBelowMove = Vector2.zero;//Exhaust the below movement expended so we don't do it twice

        if (MapData.CheckTileSetForCollision(MapData.mD.wallsMap, moveToInt))
        {//Hit a wall at the intended space
            return false;
        }

        TeleporterObject foundTeleporterAtPosition = null;

        //======[CHECKING FOR PUSHING]=========
        if (blocking)//Must be blocking to push other things
        {
            for (int i = 0; i < GameController.gC.levelObjects.Count; i++)
            {//Search through all objects
                if (GameController.gC.levelObjects[i] !=  this)//Don't push self
                {
                    if (GameController.gC.levelObjects[i] as MoveableObject)
                    {
                        MoveableObject mO = GameController.gC.levelObjects[i] as MoveableObject;
                        if (mO.blocking)//Some objects are moveable, but aren't moved by anything except below objects (can't be pushed)
                        {
                            if (Vector3.Distance(mO.GetEndPos(), moveTo) < .1f)//We are moving into this mO's space with this potential move
                            {
                                if (belowMovement && mO.fromBelowMove.magnitude > .1f)//The mO is also experiencing below movement this frame
                                {
                                    if (mO.Move(mO.fromBelowMove, true))
                                    {
                                        continue;//No pushing, if object successfully moves from it's own below movement
                                    }
                                }


                                if (!mO.Move(dir, false))//Try to push the object
                                {
                                    //Pushing returned null, meaning the object we are pushing couldn't move further, meaning we shouldn't be allowed to move here
                                    return false;
                                }
                            }
                        }
                    }
                    else//When not a MoveableObject
                    if (Vector3.Distance(GameController.gC.levelObjects[i].transform.position, moveTo) < 1f)
                    {//Found regular (non-moveable) object at position
                        if (GameController.gC.levelObjects[i] as TeleporterObject)//Object we found is a teleporter
                        {
                            foundTeleporterAtPosition = GameController.gC.levelObjects[i] as TeleporterObject;//Save this so we know to teleport during this move if our move is successful (teleporter not blocked by another object0
                        }
                        if (GameController.gC.levelObjects[i].blocking)
                        {//If wall return false
                            return false;
                        }
                    }
                }
            }
        }

        //=====!Getting this far means the movement is actually happening!
        
        //Set verb of this movement to remember for looping purposes:
        if (dir.x > .1f)
            SetVerb(Verbs.MoveRight);
        else
            if (dir.x < -.1f)
            SetVerb(Verbs.MoveLeft);
        else
            if (dir.y > .1f)
            SetVerb(Verbs.MoveUp);
        else
            if (dir.y < -.1f)
            SetVerb(Verbs.MoveDown);


        //Store the successful move as a movement used this turn
        movementsThisTurn.Add(new Movement(startPos, moveTo, false));
        
        //Resolving any Teleportation:
        if (foundTeleporterAtPosition != null)//We are on a teleporter
        {
            //See if teleporter blocked
            bool isBlocked = false;
            for (int i = 0; i < GameController.gC.levelObjects.Count; i++)
            {//Search through all objects
                if (GameController.gC.levelObjects[i].blocking)//Is blocking
                {
                    //This object is in the space teleporter would send us to
                    if (Vector3.Distance(GameController.gC.levelObjects[i].transform.position, foundTeleporterAtPosition.connectedTeleporter.transform.position) < .1f)
                    {
                        isBlocked = true;//Cache that we were blocked
                        break;
                    }
                }
            }

            if (!isBlocked)//Space teleporter is going to is not blocked
            {
                //Tell teleporter that we did this teleportation:
                foundTeleporterAtPosition.connectedTeleporter.SetTeleportationDetails(this, dir);
                //Add a movement that is basically: go from one teleporter to the other
                movementsThisTurn.Add(new Movement(foundTeleporterAtPosition.transform.position, foundTeleporterAtPosition.connectedTeleporter.transform.position, true));
                //Try another movement that moves off of the teleported to, in the direction we teleported into!
                Move(dir, belowMovement);
            }
        }
        return true;
    }
    
    /// <summary>
    /// This function is called by GC on all MoveableObjects after the Player has moved. It gives the MoveableObjects a chance to do any looping movement and also
    /// register which switches are covered by the MO at its ending position.
    /// </summary>
    public override void MoveFrame()
    {
        if (markedDead)
            return;

        //Do looping movement even if pushed
        if (looping)
        {
            switch (lastVerb)
            {
                case Verbs.MoveUp:
                    Move(Vector2.up, false);
                    break;

                case Verbs.MoveDown:
                    Move(Vector2.down, false);
                    break;

                case Verbs.MoveLeft:
                    Move(Vector2.left, false);
                    break;

                case Verbs.MoveRight:
                    Move(Vector2.right, false);
                    break;

                case Verbs.Resting:
                    break;

                case Verbs.Archer:
                    break;

                default:
                    Debug.LogError("Unknown verb for looping object:" + gameObject.name);
                    break;
            }
        }

        Vector3 endPos = GetEndPos();
        //After calculating moving, tell any switches we're going to be on top of to turn on:
        for (int i = 0; i < GameController.gC.levelObjects.Count; i++)
        {//Search through all objects
            if (GameController.gC.levelObjects[i] != this)//Don't push self
            {
                if (Vector3.Distance(GameController.gC.levelObjects[i].transform.position, endPos) < .1f)
                {//Found object at position
                    

                    if (GameController.gC.levelObjects[i].coverEvent)
                    {
                        GameController.gC.levelObjects[i].SetCoveredOnThisFrame(this);
                        break;
                    }
                }
            }
        }

    }
    public override void BeginFrame()
    {
        //Total the number of movements we take that aren't teleportation ones:
        nonTeleportMovementsCount = 0;

        for (int i = 0; i < movementsThisTurn.Count; i++)
        {
            if (!movementsThisTurn[i].teleportation)
                nonTeleportMovementsCount++;
        }

        base.BeginFrame();
    }

    public override void SetFramePos(float lerp)
    {
        if (movementsThisTurn.Count == 0)
            return;//No movement to animate
        
        //Split up based on each movement this turn, dividing up the lerp and making it happen over time (one movement = full lerp 0-1, two movements = half (0-.5 & .5-1) etc.
        float movementLength = 1f / nonTeleportMovementsCount;
        int indexes = Mathf.CeilToInt(lerp / movementLength) - 1;
        int useIndex = -1;
        for (int i = 0; i < indexes+1; i++)
        {
            useIndex++;
            if (movementsThisTurn[i].teleportation)
            {
                useIndex++;//Skip teleportation
            }
        }


        Vector3 useStart = movementsThisTurn[useIndex].startPos;
        Vector3 endPos = movementsThisTurn[useIndex].endPos;
        /*
        Vector3 useStart = startPos;
        for (int i = 0; i < movementAnimating; i++)
        {
            useStart += new Vector3(movementsThisTurn[i].x, movementsThisTurn[i].y, 0f);
        }
        Vector3 endPos = useStart + new Vector3(movementsThisTurn[movementAnimating].x, movementsThisTurn[movementAnimating].y, 0f);
        */

        float thisMovementLerp = (lerp - indexes * movementLength) / movementLength;
        transform.position = Vector3.Lerp(useStart, endPos, thisMovementLerp);
        
    }


    public override void EndFrame()
    {

        if (markedDead)
            return;

        //We cache that our current verb is resting if we didn't move this turn
        if (movementsThisTurn.Count == 0)
        {
            SetVerb(Verbs.Resting);
        }else
        {//Make sure the object ends at last position of movement (this is sometimes a problem if teleporting is the last movement)
            transform.position = movementsThisTurn[movementsThisTurn.Count - 1].endPos;
        }
        
        //Clear/reset values so ready for next frame:
        movementsThisTurn.Clear();
        startPos = transform.position;
        fromBelowMove = Vector2.zero;
        teleportSpawnSickness = false;

        CheckOverHole();
    }

    /// <summary>
    /// Checks for a hole underneath this object -- will cause cracks to crack and/or the object to fall into a hole.
    /// </summary>
    protected void CheckOverHole()
    {
        //Get our grid position:
        Vector3Int intPos = Utility.V3ToV3Int(transform.position);

        bool foundCrack = false;
        if (MapData.mD.holeCoversMap != null) {
            if (MapData.CheckTileSetForCollision(MapData.mD.holeCoversMap, intPos)){//If we find a crack
                foundCrack = true;
                if (heavyAndCrackTiles)//If we break cracks when we go over them
                    MapData.DestroyTileAtPosition(MapData.mD.holeCoversMap, intPos);//Destroy it
            }
        }

        if (!foundCrack || heavyAndCrackTiles)//If we found a crack and we aren't heavy, don't check for holes
        {
            if (MapData.CheckTileSetForCollision(MapData.mD.holesMap, intPos))
            {//Moved onto a hole
                OverHoleBegin();
            }
        }


    }

    public override void CancelSpellOn()
    {
        base.CancelSpellOn();
        CheckOverHole();//Moveable object does a quick hole check after a spell cancel, in case freeing it makes it go into a hole
    }

    
    public override void Crush()
    {
        Destroy(gameObject);
    }

    /// <summary>
    /// Sets up and triggers animation for falling into a hole
    /// </summary>
    public virtual void OverHoleBegin()
    {
        markedDead = true;
        blocking = false;//no longer blocking
        castable = false;//no longer castable
        StartCoroutine(FallInHoleAnim());
        Invoke("OverHoleEnd", .5f);
    }

    /// <summary>
    /// Coroutine that animates the object falling into a hole and going away
    /// </summary>
    /// <returns></returns>
    IEnumerator FallInHoleAnim()
    {
        float lerp = 0f;
        Vector3 startScale = transform.localScale;
        Color startColor = spriteRenderer.color;
        while(lerp < 1f)
        {
            //Animate the scale of the object to shrink and its sprite tint to move towards black as it falls:
            lerp = Mathf.Lerp(lerp, 1f, Time.deltaTime * 10f);
            transform.localScale = Vector3.Lerp(startScale, Vector3.zero, lerp);
            spriteRenderer.color = Color.Lerp(startColor, Color.black, lerp);

            yield return null;
        }
    }

    public virtual void OverHoleEnd()
    {
        Destroy(gameObject);//By default, objects are destroyed when pushed over a hole
    }

}
