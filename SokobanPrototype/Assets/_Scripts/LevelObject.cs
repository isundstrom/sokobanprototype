﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//List of all possible actions that could be looped on objects:
public enum Verbs { MoveLeft, MoveRight, MoveUp, MoveDown, Resting, Archer};

/// <summary>
/// Base class for objects in the level. Many are castable and can be looped, but this class is also used for switches which are not
/// </summary>
public class LevelObject : MonoBehaviour
{
    //Properties:
    public bool blocking;//Whether the object has collisions or not (stops the player or other object's entering its grid square)
    public bool castable;//Whether or not the object can have looping magic cast on it
    public bool crushable;//Whether the object can be crushed by falling doors or arrows
    public bool archerTarget;//Whether the object causes archers to turn and shoot at it if moved into the archer's line of sight or not
    public bool coverEvent;//Whether the object causes something to happen when covered by a crate or not (such as a switch)

    [HideInInspector]
    public Verbs lastVerb = Verbs.Resting;//The last action the object took, which can be locked if set to loop.

    public SpriteRenderer verbIcon;//Reference to a child object that displays the locked "looping" verb. LevelObjects that aren't castable don't need this

    public Sprite[] verbSpriteIcons;//Array of all the verb icon sprites this object uses based on its possible actions
    
    public SpriteRenderer spriteRenderer;

    [HideInInspector]
    public bool markedDead = false;//Called if the object is going to be destroyed soon (probably doing animation) and shouldn't be allowed to take actions


    protected bool looping;//Whether the object is currently under the effect of looping magic and repeating its action or not

    public virtual void Awake()
    {
        if (verbIcon != null)
        {
            //Set position of verb icon in corner
            verbIcon.transform.position = transform.position + new Vector3(.2f, .2f, 0f);
            //Make sure verb icons ordered relative to the object's sprite, so that behind or in front of other ones as expected
            verbIcon.GetComponent<SpriteRenderer>().sortingOrder = verbIcon.GetComponent<SpriteRenderer>().sortingOrder + spriteRenderer.sortingOrder;
        }

        //Register self to list of all levelObjects:
        GameController.gC.levelObjects.Add(this);
    }


    // Start is called before the first frame update
    public virtual void Start()
    {
        //Make sure verb icon's color is off:
        if (verbIcon != null)
            verbIcon.color = Color.gray;
    }

    
    protected virtual void OnDestroy()
    {
        //Remove from levelObjects list when leaving the scene:
        GameController.gC.levelObjects.Remove(this);
    }

    public virtual IEnumerator ResolveTurn()
    {

        yield break;

    }

    public virtual IEnumerator ResolveTurnLater()
    {

        yield break;

    }


    /// <summary>
    /// Called when the player casts looping magic on this object as a target
    /// </summary>
    public virtual void CastSpellOn()
    {
        //Don't allow casting on objects in the process of being destroyed
        if (markedDead)
            return;

        if (!castable)
        {
            Debug.LogError("Somehow casted at a not castable object!" + gameObject.name);
            return;
        }

        //Turn on verb icon and set to loop last action:
        ToggleVerbIcon(true);//Turn on verb icon and lock
        verbIcon.color = Color.cyan;
        looping = true;
        
    }

    /// <summary>
    /// Called if looping and the player cancels the spell on us (by casting on something else or other means)
    /// </summary>
    public virtual void CancelSpellOn()
    {
        if (!looping)
        {
            Debug.LogError("Somehow cancelled cast on an object not looping?" + gameObject.name);
            return;
        }

        //Turn off verb icon and looping:
        looping = false;
        ToggleVerbIcon(false);
        verbIcon.color = Color.gray;
    }

    /// <summary>
    /// Called to display the verb icon (will be ignored if already looping to prevent turning off)
    /// </summary>
    /// <param name="on"></param>
    public void ToggleVerbIcon(bool on)
    {
        if (verbIcon != null && !looping)
        {
            verbIcon.enabled = on;
        }
    }

    /// <summary>
    /// Set the object's verb based on the last action taken
    /// </summary>
    /// <param name="verb"></param>
    protected virtual void SetVerb(Verbs verb)
    {
        if (!castable)//Non castable objects don't have verbs
            return;

        if (looping)//Can't change verb when looping
            return;

        //Remember the verb for looping purposes
        lastVerb = verb;

        //Set the verb sprite based on verb
        verbIcon.sprite = verbSpriteIcons[(int)(verb)];//Kinda garbage to cast enum to int, because if you move movement verbs from start this won't work anymore
        //^^^Ideally this would probably be a dictionary of enum keys and icon sprites values stored somewhere everbody can check instead^^^
    }

    /// <summary>
    /// Called by the GameController in situations the object can be crushed
    /// </summary>
    public virtual void Crush()
    {
        markedDead = true;
    }

    /// <summary>
    /// Called by the GameController to calculate all moveable objects movements this frame, where they start and end and whether they push any other objects from their own movement+
    /// </summary>
    public virtual void MoveFrame()
    {

    }

    /// <summary>
    /// Called by GC after all object movements are calculated, but before the actual animation of movement happens
    /// </summary>
    public virtual void BeginFrame()
    {

    }

    /// <summary>
    /// Called by GC to animate the progress of the object changing over time from the frame
    /// </summary>
    /// <param name="lerp">Value used to lerp between animations for the object, range [0f-1f]</param>
    public virtual void SetFramePos(float lerp)
    {

    }

    /// <summary>
    /// Called by GC after all frame animation has finished, used to finalize variables about state
    /// </summary>
    public virtual void EndFrame()
    {

    }

    public virtual void SetCoveredOnThisFrame(MoveableObject callingMO)//callingMO is used by teleporter to clone the mO
    {

    }

}
