﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A level object that spots other objects in the scene (including the player) and shoots at them the next frame
/// </summary>
public class ArcherObject : MoveableObject
{
    enum ArcherStates { Waiting, Spotting, Firing};//The possible states of the archer
    
    public GameObject exclamationMark;//Extra icon graphic that gets toggled on and off above the archer's head when they spot things
    public GameObject arrowPrefab;//Prefab we clone to make arrows
    
    public Transform graphic;//The child graphic of the archer that gets turned to face in different directions

    ArcherStates currentState = ArcherStates.Waiting;//What state the archer is currently in
    Vector3 facedDir = Vector3.up;//Where the archer is currently facing

    GameObject arrow;//Referenced to the spawned arrow object this turn


    LevelObject objectShotThisTurn;//Stored reference to the object that will be shot by the archer this turn
    Vector3 arrowEndPointThisTurn;//Where the arrow's flight path ends this turn

    public override void BeginFrame()
    {
        //After moving object positions are resolved the archer looks in four directions for any targets it should fire at

        if (markedDead)
            return;

        int checkDist = 15;//Max range in any direction to spot things
        bool[] finishedDir = new bool[4];//Whether we have found something in a given cardinal direction yet (will keep looping further and further until true)

        if ((currentState == ArcherStates.Waiting || currentState == ArcherStates.Firing) && !looping)
        {
            //============[DETECTING TARGETS]=============
            LevelObject foundTarget = null;//The target found if one is found
            //Basically do a grid based ray cast, checking every position in all four directions until we hit a blocker or a wall
            for (int i = 1; i < checkDist; i++)
            {
                if (!finishedDir[0])
                    finishedDir[0] = CheckForTarget(transform.position + Vector3.left * i, out foundTarget);//Check spaces to left
                if (!finishedDir[1] && foundTarget == null)
                    finishedDir[1] = CheckForTarget(transform.position + Vector3.up * i, out foundTarget);//Check spaces upwards
                if (!finishedDir[2] && foundTarget == null)
                    finishedDir[2] = CheckForTarget(transform.position + Vector3.right * i, out foundTarget);//Check spaces to right
                if (!finishedDir[3] && foundTarget == null)
                    finishedDir[3] = CheckForTarget(transform.position + Vector3.down * i, out foundTarget);//Check spaces downwards

                if (foundTarget != null)//We found a target
                    break;

                if (finishedDir[0] && finishedDir[1] && finishedDir[2] && finishedDir[3])
                    break;//Finished all directions, so break out of for loop
            }

            if (foundTarget != null)//We found a target
            {
                Vector3 useTargetPos = foundTarget.transform.position;
                if (foundTarget as MoveableObject)
                    useTargetPos = (foundTarget as MoveableObject).GetEndPos();
                //Look direction:
                facedDir = (useTargetPos - transform.position).normalized;

                SetArcherState(ArcherStates.Spotting);//Set archer state to indicate we should shoot in the direction we spotted something next frame
                exclamationMark.SetActive(true);//During the Spotting frame display an exclamation point above our head
            }
            else
            {
                SetArcherState(ArcherStates.Waiting);//If we didn't find any targets, stay in waiting state
            }
            
        }else
        if ((currentState == ArcherStates.Spotting && !looping) || (currentState == ArcherStates.Firing && looping))
        {
            //=============[SHOOTING]=============
            exclamationMark.SetActive(false);

            SetArcherState(ArcherStates.Firing);

            //====Spawn the arrow prefab====
            //Find current target if any:
            bool hitSomething = false;
            objectShotThisTurn = null;//The target found if one is found
            //Calculate the end position of shooting in the direction we're facing:
            for (int i = 1; i < checkDist; i++)//Keep looping to our max check distance
            {
                hitSomething = CheckForTarget(transform.position + facedDir * i, out objectShotThisTurn);//Check spaces to left
                if (hitSomething)//If we hit something in this direction
                {
                    arrowEndPointThisTurn = transform.position + facedDir * i;//Save the end point of how far we shot for
                    break;
                }
            }

            if (objectShotThisTurn != null && objectShotThisTurn as Player)
                GameController.gC.timeSlowThisTurn = true;//When an archer shoots the player, do cool time dilation (slow down as arrow flies)

            //Create the actual arrow prefab
            arrow = Instantiate(arrowPrefab, transform.position, graphic.rotation);

        }

        base.BeginFrame();
    }

    public override void SetFramePos(float lerp)
    {

        if (markedDead)
            return;

        if (currentState == ArcherStates.Spotting)
        {//Rotate towards the target we spotted
            graphic.rotation = Quaternion.Lerp(graphic.rotation, Quaternion.LookRotation(Vector3.forward, facedDir), lerp);
            exclamationMark.transform.localPosition = Vector3.Lerp(Vector3.zero, Vector3.up * .6f, lerp);//Make exclamation point animate, rising above head
        }

        if (currentState == ArcherStates.Firing)
        {
            //Only animate moving arrow flying through air (no real game logic object interaction)
            arrow.transform.position = Vector3.Lerp(transform.position, arrowEndPointThisTurn, lerp);
        }

        base.SetFramePos(lerp);//Do movement stuff if pushed
    }

    public override void EndFrame()
    {

        if (markedDead)
            return;

        if (currentState == ArcherStates.Firing)
        {
            //Resolve arrow hitting things (kill player, break crates, etc)

            if (objectShotThisTurn != null)
            {
                //If the object we hit can be broken, cause it to be
                if (objectShotThisTurn.crushable)
                {
                    objectShotThisTurn.Crush();
                    Destroy(arrow, .5f);
                }
            } else
            {
                //Trigger an animation on the arrow that makes it fly up in the air and spin
                SpinAnimation spinScript = arrow.GetComponent<SpinAnimation>();
                spinScript.StartCoroutine(spinScript.DoSpinAnimation(1f,1f,1f, true));
            }
        }

        base.EndFrame();
    }

    /// <summary>
    /// Checks a position for something the arrow would hit. If it is not open from a wall or a blocking object, it returns false. If it finds a target returns it in foundTarget (otherwise null)
    /// </summary>
    /// <param name="position">The position to check</param>
    /// <param name="foundTarget">Out parameter of a LevelObject hit by the arrow (can be null if we hit a wall instead)</param>
    /// <returns></returns>
    bool CheckForTarget(Vector3 position, out LevelObject foundTarget)
    {
        Vector3Int posInt = Utility.V3ToV3Int(position);

        foundTarget = null;

        if (MapData.CheckTileSetForCollision(MapData.mD.wallsMap, posInt))
        {//Hit a wall
            return true;
        }
        for (int i = 0; i < GameController.gC.levelObjects.Count; i++)
        {//Search through all objects
            Vector3 posToCompare = GameController.gC.levelObjects[i].transform.position;
            if (GameController.gC.levelObjects[i] as MoveableObject)//Moving objects need to use where they WILL be as the archer spotting, not where they are
            {
                posToCompare = (GameController.gC.levelObjects[i] as MoveableObject).GetEndPos();
            }
            if (Vector3.Distance(posToCompare, position) < .1f)
            {//Found object at position
                if (GameController.gC.levelObjects[i].blocking)//Is a blocking object
                {
                    if (GameController.gC.levelObjects[i].archerTarget)
                    {
                        foundTarget = GameController.gC.levelObjects[i];

                    }

                    return true;
                }
            }
        }

        return false;
    }

    public override void CastSpellOn()
    {
        lastVerb = Verbs.Archer;//This doesn't actually do anything, archer functions on archer states, BUT this does stop movemable object inheritance getting blocked while moving
        base.CastSpellOn();
    }
    

    void SetArcherState(ArcherStates setState)
    {
        currentState = setState;

        //Set the verb sprite based on state
        verbIcon.sprite = verbSpriteIcons[(int)(setState)];//Kinda garbage to cast enum to int, because if you move movement verbs from start this won't work anymore
    }

    protected override void SetVerb(Verbs verb)
    {
        //Archer does not use verb setting, but instead uses SetArcherState
        return;
    }

}
