﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// GameController class that ensures all interactions between objects happen in an orderly way. Big ResolveObjectTurns coroutine goes through each set of objects in phases
/// and calculates how each object will behave during the one frame or turn of game play. Once everything is resolved, it then animates everything happening to the objects
/// within the frame all at once.
/// </summary>
public class GameController : MonoBehaviour
{
    public static GameController gC;//Singleton reference to the GC
    public List<Player> playerList;//List of all Players (because, yes, there can't be more than one player thanks to cloning mechanic)
    public List<LevelObject> levelObjects;//List of all LevelObjects in the scene
    public List<ConveyorObject> conveyorObjects;//List of all Conveyors in the scene
    public List<TeleporterObject> teleporterObjects;//List of all teleporters in the scene

    public Transform stairsObject;//Reference to the level's stair object. Player must reach this to complete the level.

    public bool resolvingTurns;//Whether currently resolving the frame or animating, blocks any further input until this flags back to false


    [HideInInspector]
    public bool timeSlowThisTurn = false;//Adds drama when action things happen in the frame, like shooting arrows, do some time dilation

    //Movement/Input:
    Vector2 lastInput = Vector2.zero;//Cache of the input received the previous frame
    
    bool loadingLevel = false;//Whether we are currently loading a new level (blocks input during)

    float allPlayersDeadFrames = 0;//How many frames have passed since every Player has been destroyed, if we reach a certain number trigger a GameOver
    

    // Start is called before the first frame update
    void Awake()
    {
        gC = this;//Fill singleton reference

        //Construct empty lists:
        levelObjects = new List<LevelObject>();
        conveyorObjects = new List<ConveyorObject>();
        teleporterObjects = new List<TeleporterObject>();
        
    }

    private void Update()
    {
        //====[General hotkeys]====
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            GameOver();//Restart level
        }

        //Skip a level
        if (Input.GetKeyDown(KeyCode.PageDown))
        {
            ChangeLevel(1);
        }

        //Back a level
        if (Input.GetKeyDown(KeyCode.PageUp))
        {
            ChangeLevel(-1);
        }
        

        //====[CHECK FOR GAME OVER]=======
        if (playerList.Count == 0)
        {
            allPlayersDeadFrames++;
            if (allPlayersDeadFrames > 3)
                GameOver();//Prevents doing a game over AFTER game has been exited while destroying everything
        }


        //====[READ MOVEMENT INPUT]=========
        HandleMovementInput();
    }


    public void LateUpdate()
    {
        //Position camera to be focused on the first player in the list (no need for fancy code that averages all cloned player positions yet)
        if (playerList.Count > 0)
        {
            Camera.main.transform.position = new Vector3(playerList[0].transform.position.x, playerList[0].transform.position.y, Camera.main.transform.position.z);
        }
        
    }
    
    /// <summary>
    /// Resolves the input for moving the Player
    /// </summary>
    public void HandleMovementInput()
    {
        if (playerList.Count == 0)
            return;//No movement if no players

        Vector2 moveInput = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));

        float dead = .9f;//How much input from an axis to count as input (although this isn't really necessary since it looks like I'm using GetAxisRaw???)

        Vector2 orthoMove = Vector2.zero;//Cache the intended orthogonal (left, right, up, down) movement based on current input

        if (Mathf.Abs(moveInput.x) > dead)
        {
            orthoMove = new Vector2((moveInput.x > 0f) ? 1f : -1f, 0f);
        }
        else
        if (Mathf.Abs(moveInput.y) > dead)
        {
            orthoMove = new Vector2(0f, (moveInput.y > 0f) ? 1f : -1f);
        }

        //Only count movement input if there is a difference betwen this frames input and last frame (ignore if identical), acts like OnKeyDown
        if ((lastInput - orthoMove).magnitude > Mathf.Epsilon)
        {
            lastInput = orthoMove;

            if (resolvingTurns)//No input while still resolving a turn
                return;

            if (orthoMove.magnitude > Mathf.Epsilon)//Actually moving (not just releasing key)
            {
                if (!playerList[0].selectingTarget)//If not selecting a target, go ahead and resolve the turn using the player's input
                {
                    
                    StartCoroutine(ResolveObjectTurns(orthoMove));
                }
                else
                {//If the player is selecting a target, move the selector not the player
                    playerList[0].MoveTargetter(orthoMove);
                }

                return;
            }
        }


        if (resolvingTurns)
            return;


        //Left click opens casting targetting
        if (Input.GetMouseButtonDown(0))
        {
            if (playerList[0].SelectingTargetClicked())//If returns true, did something that moves turn forward
            {
                StartCoroutine(ResolveObjectTurns(Vector2.zero));//Resolve turns with no movement
            }
        }

        //Move the casting selector based on the mouse position ont eh screen when casting open:
        if (playerList[0].selectingTarget)
        {
            Vector2 mousePos = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0f));
            mousePos = new Vector2(Mathf.Round(mousePos.x), Mathf.Round(mousePos.y));
            playerList[0].MoveTargetterWithMouse(mousePos);//Place casting targetting square at mouse position in grid
        }
    }

    /// <summary>
    /// Called when a player is killed to remove them from the playerList
    /// </summary>
    /// <param name="toRemove"></param>
    public void RemovePlayer(Player toRemove)
    {
        
        playerList.Remove(toRemove);
        
    }

    /// <summary>
    /// This coroutine controls most objects in the game. Looping through them at different phases for one "frame"  of movement a turn, animating, and resolving
    /// </summary>
    /// <param name="playerMovement">We pass in the players movement direciton this turn (if any) which is used to resolve the turn</param>
    /// <returns></returns>
    public IEnumerator ResolveObjectTurns(Vector2 playerMovement)
    {
        resolvingTurns = true;//Block any further input from player until we are done
        float speedMod = 8f;//Increase or lower this value to make the turn animation hapepn faster
        float lerp = 0f;//Progress through turn animation [0f-1f]

        //======[RESOLVE PLAYER MOVEMENT PT. 1]==========
        if (playerMovement.magnitude > .1f)//Some movement put in for player so do this part:
        {
            for (int i = 0; i < playerList.Count; i++)
            {
                if (!playerList[i].teleportSpawnSickness && !playerList[i].markedDead)//Don't pass input to player if it just spawned from a teleporter looping
                    playerList[i].fromBelowMove = playerMovement;//We actually use the same movement as conveyors to move the player on turns they moved
            }
        }

        //======[RESOLVE TELEPORTATION]==========
        //Loop through and tell all teleporters to spawn any objects they need to (only happens if the teleporter is locked to loop)
        for (int i = 0; i < teleporterObjects.Count; i++)
        {
            if (teleporterObjects[i].gameObject.activeInHierarchy)
                teleporterObjects[i].TeleporterSpawnFrame();
        }

        
        //======[RESOLVE PLAYER MOVEMENT PT. 2]==========
        if (playerMovement.magnitude > .1f)//Some movement put in for player so do this part:
        { 
            for (int i = 0; i < playerList.Count; i++)
            {
                //Turn the playurs in their correct direction and have them do the "below movement" set in PT. 1 above
                if (!playerList[i].teleportSpawnSickness && !playerList[i].markedDead)//Don't pass input to player if it just spawned from a teleporter looping
                {
                    playerList[i].graphic.rotation = Quaternion.LookRotation(Vector3.forward, playerMovement);

                    playerList[i].DoBelowMovement();
                }
            }
        }

        //======[CALL MOVEFRAME ON ALL LEVEL OBJECTS]==========
        for (int i = 0; i < levelObjects.Count; i++)
        {
            //For MoveableObjects this calculates each movement they will be taking this frame and anything they push or act on
            if (levelObjects[i].gameObject.activeInHierarchy)
                levelObjects[i].MoveFrame();
        }
        
        //======[FIND OBJECTS THAT ARE ON CONVEYORS]==========
        for (int i = 0; i < conveyorObjects.Count; i++)
        {
            conveyorObjects[i].ConveyorGetRestingObject();
        }
        
        //======[TELL CONVEYORS TO ADD MOVEMENTS TO OBJECTS THAT ARE ON THEM]==========
        for (int i = 0; i < conveyorObjects.Count; i++)
        {
            conveyorObjects[i].ConveyorFrame();
        }

        
        //======[PREP ALL OBJECTS TO PREPARE FOR FRAME ANIMATION]==========
        for (int i = 0; i < levelObjects.Count; i++)
        {

            if (levelObjects[i].gameObject.activeInHierarchy)
                levelObjects[i].BeginFrame();
        }
        
        //~At this point, all moving this frame has been resolved logically and end positions for the frame are accurate, we just haven't animated yet


        
        //======[ACTUALLY ANIMATE THE OBJECTS AS IF THEY ARE ALL MOVING/CHANGING AT ONCE]==========
        while (lerp < 1f)
        {
            float speedDilated = speedMod;
            if (timeSlowThisTurn && lerp > .5f)
                speedDilated = Mathf.Lerp(speedMod, speedMod * .05f, (lerp-.5f)*2f);

            lerp = Mathf.MoveTowards(lerp, 1f, Time.deltaTime * speedDilated);
            for (int i = 0; i < levelObjects.Count; i++)
            {
                if (levelObjects[i].gameObject.activeInHierarchy)
                    levelObjects[i].SetFramePos(lerp);
            }

            yield return null;
        }

        //======[TELL ALL OBJECTS THAT THE FRAME'S ANIMATION IS COMPLETE]==========
        for (int i = 0; i < levelObjects.Count; i++)
        {

            if (levelObjects[i].gameObject.activeInHierarchy)
                levelObjects[i].EndFrame();
        }

        //Mark that the frame has ended and reset vars:
        resolvingTurns = false;
        timeSlowThisTurn = false;
       
    }

    /// <summary>
    /// Loops through all level objects and turns on their verb icons when the player is choosing what to cast on
    /// </summary>
    /// <param name="on"></param>
    public void ToggleVerbIcons(bool on)
    {
        for (int i = 0; i < levelObjects.Count; i++)
        {
            levelObjects[i].ToggleVerbIcon(on);
        }
    }


    /// <summary>
    /// Called when all Players are dead to indicate game over. For now simply, reloads the level
    /// </summary>
    void GameOver()
    {
        if (!loadingLevel)//If we aren't already loading a level
        {
            //Reload the current level:
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            loadingLevel = true;
        }
    }

    /// <summary>
    /// Caleld to switch between scenes
    /// </summary>
    /// <param name="change">Indicates relative difference between current scene and scene to go to in build index (1 indicates goes to next scene, -1 indicates go to previous scene)</param>
    public void ChangeLevel(int change)
    {
        if (loadingLevel)//Don't change if already loading a scene
            return;

        //Calculate scene index based on relative change
        int sceneIndex = SceneManager.GetActiveScene().buildIndex + change;

        //Make sure sceneIndex is a valid scene within build settings:
        if (sceneIndex >= 0 && SceneManager.sceneCountInBuildSettings > sceneIndex)
        {
            SceneManager.LoadScene(sceneIndex);//Load the sceen
            loadingLevel = true;
        }
        else
            Debug.LogError("YOU WON! [no scenes from level in build index]");
    }
}
