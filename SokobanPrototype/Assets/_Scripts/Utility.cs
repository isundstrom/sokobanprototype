﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Static utilty functions
/// </summary>
public static class Utility
{
    /// <summary>
    /// Converts a Vector3 into a Vector3Int by rounding floats of x,y,z to ints
    /// </summary>
    /// <returns></returns>
    public static Vector3Int V3ToV3Int(Vector3 v3)
    {
        return new Vector3Int(Mathf.RoundToInt(v3.x), Mathf.RoundToInt(v3.y), Mathf.RoundToInt(v3.z));
    }

    /// <summary>
    /// Given a color, multiplies through its R, G, and B values by a given tint value, effectively allowing you to make tints and shades of the color easily
    /// </summary>
    /// <param name="original"></param>
    /// <param name="tint"></param>
    /// <returns></returns>
    public static Color TintColor(Color original, float tint)
    {
        return new Color(original.r * tint, original.g * tint, original.b * tint, original.a);
    }
}
